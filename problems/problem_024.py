# Complete the calculate_average function which accepts
# 1 a list of numerical values and returns the average of
# the numbers.
#
#2  If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you
#len list -> sum list -> sum list/len list ->return ave
def calculate_average(values):
    values_length = len(values)
    sum_values = 0
    if values_length == 0:
        return None
    else:
        for num in values:
            sum_values += num
        avg = sum_values/values_length
        return avg


list =[1,2]

print(calculate_average(list))
