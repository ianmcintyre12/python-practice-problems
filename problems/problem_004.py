# Complete the max_of_three function so that returns the
# maximum of three values.
#obj1
# If two values are the same maximum value, return either of
# them.
# obj2
# If the all of the values are the same, return any of them
#obj3
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md
# 3 sep returns
#     one big if for all 3
#     if/elif for pairs
#     maximum return
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    list_values = [value1,value2,value3]
    list_values_ascending = sorted(list_values)
    if value1 == value2 == value3:
         return value1
    elif value1== value2 and value1 > value3:
         return value1
    elif value2 == value3 and value2> value1:
         return value2
    elif value1 == value3 and value1 > value2:
         return value3
    else:
         return list_values_ascending[-1]
# value1 = 4
# value2 = 6
# value3 = 7

# min_num = max_of_three(value1,value2,value3)
# print(min_num)
