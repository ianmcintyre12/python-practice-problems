# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# 1If the list of values is empty, the function should
# return None
#
# 2If the list of values has only one value, the function
# should return None
#list non repeating values -> sort ->list[-2]
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) <=1:
        return None
    new_val = []
    for num in values:
        if num not in new_val:
            new_val.append(num)
    new_val.sort()
    return new_val[-2]


list = [1,1,1,3,2,4,2]

print(find_second_largest(list))
