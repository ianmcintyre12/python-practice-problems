# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]
#obj split list in half into two separate lists
#len list/2 to find index -> append in range 0 -> half in list 1, append in range half -> len(list) list2

def halve_the_list(list):
    half_point = len(list)/2
    first_half =[]
    second_half =[]
    if len(list)/2 % 1:
        half_point = half_point + 0.5

    for i in range(int(half_point)):
        first_half.append(list[i])
    for i in range(int(half_point),len(list)):
        second_half.append(list[i])
    return first_half, second_half

list = [1,2,3]
print(halve_the_list(list))
