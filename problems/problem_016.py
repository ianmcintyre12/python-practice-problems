# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.
# set bound  0<x<10 and 0<y<10
#both =true then return true
#else false
def is_inside_bounds(x, y):
    if 0 < x and x < 10:
        if 0 < y and y < 10:
            return True
        else:
            return False
    else:
        return False

x = 7
y = 0

print(is_inside_bounds(x,y))
