# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md
# check if /3 by r =0
# Write out some pseudocode before trying to solve the

#   if num % 3 = 0 true else false
# problem to get a good feel for how to solve it.

def is_divisible_by_3(number):
    if number % 3 == 0:
        return 'fizz'
    else:
        return number

num=4
print(is_divisible_by_3(num))
