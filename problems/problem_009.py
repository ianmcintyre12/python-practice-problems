# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md
#string -> list -> reversee -> join -> string == string reversed
# Write out some pseudocode before trying to solve the
#word='x'
#x.reverse = []
##[].join
#word ==[]
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    word_list = list(word)
    word_list.reverse()
    word_reverse = (''.join(word_list))
    if word == word_reverse:
        return True
    else:
        return False

# word='olll'
# print(is_palindrome(word))
