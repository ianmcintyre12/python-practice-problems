# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"
#obj create valid password
# dif checks = 0, if check is met add 1, if all checks>0 pass else fail
def check_password(password):
    lower_check = 0
    upper_check = 0
    digit_check = 0
    special_character = 0
    len_check = 0
    if len(password) >= 6 and len(password) <= 12:
        len_check +=1

    for char in str(password):
        if char == '$' or char == '!' or char == '@':
            special_character += 1

    for char in str(password):
        if char.isdigit():
            digit_check += 1

    for char in str(password):
        if char.isupper():
            upper_check +=1

    for char in str(password):
        if char.islower():
            lower_check +=1

    if (lower_check > 0 and upper_check >0 and
        digit_check > 0 and special_character > 0
        and len_check >0):
        return True
    else:
        return False


p = '12a!Bss3'

print(check_password(p))
