# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#1 remove dupes
# If the list is empty, then return the empty string.
# list of counted -> run thru list add to counted -> if list in list counted do not add -> return counted.joined
def remove_duplicate_letters(s):
    new_str =[]
    for char in s:
        if char not in new_str:
            new_str.append(char)
    removed_lett = ''.join(new_str)
    return removed_lett


str='abccba'

print(remove_duplicate_letters(str))
