# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
# 1  * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
# 2  * If it is a workday, the list needs to contain "laptop"
#3   * If it is not a workday, the list needs to contain
#     "surfboard"
#list -> if not sunny list +umb -> if work +lap -> if not work +surf

def gear_for_day(is_workday, is_sunny):
    gear_list =[]
    if is_workday == True:
        gear_list.append('laptop')
    else:
        gear_list.append('surfboard')
    if is_sunny != True and is_workday == True:
        gear_list.append('umbrella')
    return gear_list


is_workday = False
is_sunny = False

print(gear_for_day(is_workday,is_sunny))
