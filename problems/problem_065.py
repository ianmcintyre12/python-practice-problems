# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

def biggest_gap(list_num):
    if len(list_num) < 2:
        return None
    big_gap = 0
    for num in range(len(list_num) - 1):
        gap = list_num[num +1] - list_num[num]
        if abs(gap) > big_gap:
            big_gap = abs(gap)
    return big_gap


nums = [3,1]

print(biggest_gap(nums))
