# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.
# check if attend/member >= .5
# if percent >= .5 return true
def has_quorum(attendees_list, members_list):
    percent_attendance = len(attendees_list)/len(members_list)
    if percent_attendance >= .5:
        return True
    else:
        return False

attendee_list=[1,2,3,4]
member_list =[1,2,3,4,5,6,7]

print(has_quorum(attendee_list,member_list))
