# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7
#obj sum two parameters
# x + y = return
def sum_two_numbers(num1, num2):
    return int(num1) + int(num2)


print(sum_two_numbers(1,2))
