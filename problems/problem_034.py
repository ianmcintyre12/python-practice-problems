# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#1 count dig
#2 count alpha
#3return alpha , dig
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#loops x thru s -> if x.isdig -> if x.isalpa ->return dig/allpha
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2
#loops x thru s -> if x.isdig -> if x.isalpa ->return dig/allpha

def count_letters_and_digits(s):
    number_of_digits = 0
    number_of_alpha = 0
    for char in s:
        if char.isalpha():
            number_of_alpha += 1
        if char.isdigit():
            number_of_digits +=1
    return number_of_alpha , number_of_digits



str =''

print(count_letters_and_digits(str))
