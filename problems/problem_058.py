# Write a function that meets these requirements.
#
# Name:       group_cities_by_state
# Parameters: a list of cities in the format "«name», «st»"
#             where «name» is the name of the city, followed
#             by a comma and a space, then the two-letter
#             abbreviation of the state
# Returns:    a dictionary whose keys are the two letter
#             abbreviations of the states in the list and
#             whose values are a list of the cities appearing
#             in that list for that state
#
# In the items in the input, there will only be one comma.
#
# Examples:
#     * input:   ["San Antonio, TX"]
#       returns: {"TX": ["San Antonio"]}
#     * input:   ["Springfield, MA", "Boston, MA"]
#       returns: {"MA": ["Springfield", "Boston"]}
#     * input:   ["Cleveland, OH", "Columbus, OH", "Chicago, IL"]
#       returns: {"OH": ["Cleveland", "Columbus"], "IL": ["Chicago"]}
#
# You may want to look up the ".strip()" method for the string.
#create a dictionary where key = state value = list of cities

#empty dict -> if k not in dict -> dict[key] = [list of cities + new citie]

def group_cities_by_state(list):
    city_dict = {}
    for item in range(len(list)):
        sep_city = list[item].split(', ')
        if sep_city[1] not in city_dict:
            city_dict[sep_city[1]] = [sep_city[0]]
        else:
            city_dict[sep_city[1]].append(sep_city[0])
    return city_dict

list = ["Springfield, MA", "Boston, MA"]
print(group_cities_by_state(list))

#split list entries by ,
# list[i].split(',') = city_state [city, state], continued
#  creates list [city, state]
#if k not in dict
#   dict[key] = [cities]
