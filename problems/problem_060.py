# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]

def only_odds(list_nums):
    list_odds = []
    for num in list_nums:
        if num % 2 == 1:
            list_odds.append(num)
    return list_odds


nums =[1,2,3,4,1]

print(only_odds(nums))
