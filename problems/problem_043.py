# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.
# obj:return index number of item in  list

# empty list -> for loop for index in range(len(list)) -> if x in list ->empty.append(index)
def find_indexes(search_list, search_term):
    result = []
    for i in range(len(search_list)):
        if search_term == search_list[i]:
            result.append(i)
    return result


search_list = ['a','b','a']
search_term = 'a'
#     result:       [3]

print(find_indexes(search_list, search_term))
