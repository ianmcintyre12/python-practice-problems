# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.
# obj1: two lists -> sum same index together -> populate one list
# for x in list 1 and list 2 -> loop sum into append -> list 3
def pairwise_add(list1, list2):
    if len(list1) != len(list2):
        return None
    result = []
    for i in range(0,len(list1)):
        result.append(list1[i] + list2[i])
    return result


list1 =  [1, 2, 3, 4]
list2 =  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]


print(pairwise_add(list1, list2))
