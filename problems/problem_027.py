# Complete the max_in_list function to find the
#1 maximum value in a list
#
# If the list is empty, then return None.
#len list = 0 -> none
#list sort -> list[-1]

def max_in_list(values):
    if len(values) == 0:
        return None
    values.sort()
    # print(values)
    return values[-1]

list = [1,2,3,4,5,3,2,1]

print (max_in_list(list))
