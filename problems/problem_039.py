# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}
#  for loop for k,v in dict.items() -> return v,k
def reverse_dictionary(dictionary):
    # reversed_dict={}
    # for k,v in dictionary.items():
    #     reversed_dict.update({ v : k})
    # return reversed_dict
    #^^^^^does not work with multiple values that are the same
    list_key =[]
    list_val =[]
    for k,v in dictionary.items():
        list_key.append(k)
        list_val.append(v)
    new_dict = dict(zip(list_val,list_key))
    return new_dict
dic ={"one": 3, "two": 2, "three": 3}


print(reverse_dictionary(dic))
