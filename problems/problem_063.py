# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(word):
    dict_alphabet = {'a':'b' ,'b':'c','c':'d','d':'e','e':'f','f':'g','g':'h','h':'i'
                     ,'i':'j','j':'k','k':'l','l':'m','m':'n','n':'o','o':'p','p':'q'
                     ,'q':'r','r':'s','s':'t','t':'u','u':'v','v':'w','w':'x','x':'y','y':'z','z':'a'}
    shifted = []
    for char in word:
        if char.islower():
            shifted.append(dict_alphabet.get(char))
        if char.isupper():
            char = char.lower()
            shifted.append(dict_alphabet.get(char).upper())
    return ''.join(shifted)


word ='Kala'

print(shift_letters(word))
