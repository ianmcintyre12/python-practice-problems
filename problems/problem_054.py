# Write a function that meets these requirements.
#
# Name:       check_input
# Parameters: one parameter that can hold any value
# Returns:    if the value of the parameter is the
#             string "raise", then it should raise
#             a ValueError. otherwise, it should
#             just return the value of the parameter
#
# Examples
#    * input:   3
#      returns: 3
#    * input:   "this is a string"
#      returns: "this is a string"
#    * input:   "raise"
#      RAISES:  ValueError
#obj check if int
# if raise -> value error
# if p == int or p == string -> return p
#
def check_input(p):
    if p == 'raise':
        raise ValueError
    if type(p) is int or type(p) is str:
        return p

p = 'strng'
print(check_input(p))
