# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#1: sum 2:return non if empy
# If the list of values is empty, the function should
# return None
# sum = 0 -> for x in list += sum -> return sum or none depend

def calculate_sum(values):
    if len(values) == 0:
        return None
    else:
        sum = 0
        for num in values:
            sum += num
    return sum

list = [0]
print(calculate_sum(list))
