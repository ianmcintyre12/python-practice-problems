# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

### 01 minimum_value

if statements  with comparing operators >/<

### 04 max_of_three
3 sep returns
    one big if for all 3
    if/elif for pairs
    maximum return
### 06 can_skydive

### continue to plan each
